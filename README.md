# R CODE: Activity of transcript factors influence and regulators in renal allograft biopsies from deceased and living donors.

Nathan JOONNEKINDT, Haris PATEL, Riyâz PATEL 

Univ. Lille, Faculté Ingénierie et Management de la Santé, F-59000, Lille, France 

## Abstract

La survie de transplantation rénale est plus élevée avec des donneurs vivants que des donneurs décédés. L’étude menée avec le package CoRegNet pour l’analyse des régulateurs d’influence et de l’activité des facteurs de transcriptions révèle une différence significative dans le temps (30 minutes versus 3 mois), indépendamment du type de donneur, en plus d’une différence initiale entre les deux groupes de donneurs. Ainsi, la différence d’activité transcriptionnelle et de régulateurs influents identifiés, entre donneurs vivants et donneurs décédés, qui évolue dans le temps post-transplantation, pourrait contribuer à la différence de survie entre allogreffes provenant de donneurs vivants ou décédés.
